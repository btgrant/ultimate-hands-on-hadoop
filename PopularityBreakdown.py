from mrjob.job import MRJob
from mrjob.step import MRStep

# Establish the popularity of a movie based on the number of ratings it receives
class PopularityBreakdown(MRJob):
    def steps(self):
        return [
          MRStep(mapper=self.mapper_count_ratings,
                 reducer=self.reducer_count_ratings)
        ]

    def mapper_count_ratings(self, _, line):
        (userID, movieID, rating, timestamp) = line.split('\t')
        yield  movieID, 1

    def reducer_count_ratings(self, key, values):
        yield key, sum(values)


if __name__ == '__main__':
    PopularityBreakdown.run()
