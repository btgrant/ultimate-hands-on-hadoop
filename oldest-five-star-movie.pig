ratings = LOAD '/user/maria_dev/ml-100k/u.data' AS (userID:int, movieID:int, rating:int, ratingTime:int);
--DESCRIBE ratings;

ratingsByMovie = GROUP ratings BY movieID;
--DUMP ratingsByMovie;
--DESCRIBE ratingsByMovie;

avgRatings = FOREACH ratingsByMovie GENERATE group AS movieID, AVG(ratings.rating) AS avgRating;
--DESCRIBE avgRatings;
--DUMP avgRatings;

fiveStarMovies = FILTER avgRatings BY avgRating > 4.0;
--DESCRIBE fiveStarMovies;

metadata = LOAD '/user/maria_dev/ml-100k/u.item' USING PigStorage('|')
    AS (movieID:int, movieTitle:chararray, releaseDate:chararray, videoRelease:chararray, imdbLink:chararray);

nameLookup = FOREACH metadata GENERATE movieID, movieTitle, ToUnixTime(ToDate(releaseDate, 'dd-MMM-yyyy')) AS releaseTime;
--DESCRIBE nameLookup;

fiveStarsWithData = JOIN fiveStarMovies BY movieID, nameLookup by movieID;
--DESCRIBE fiveStarsWithData;
--DUMP fiveStarsWithData;

oldestFiveStarMovies = ORDER fiveStarsWithData BY nameLookup::releaseTime;

DUMP oldestFiveStarMovies;
