# Follow-Ups

* [ ] Try out some of the query operations using AWS Athena to see how similar Hive queries are
* [ ] Is there a good SQL "crash course" book I can reference?
* [ ] Port some Python-based Spark code to Scala to measure performance differences

# Quick Reference

* [Ambari login](http://localhost:8080): 
    * `maria_dev`:`maria_dev` 
    * `admin`:`HortonWorks!`
* `ssh maria_dev@127.0.0.1 -p 2222`
* `root`:`HortonWorks!` (default: `hadoop`)

## Sundog Education resources

* [Hadoop materials](https://sundog-education.com/hadoop-materials/)
* ["The Ultimate Hands-On Hadoop: Tame Your Big Data" Facebook group](https://www.facebook.com/groups/1953142698341617/)

## Hadoop CLI 

* `hadoop fs`:  this will also list all options for `fs`
    * `-ls`
    * `-rm`
    * `-mkdir`
    * `-rmdir`
    * `-copyFromLocal`

# 1. Learn All The Buzzwords and Install Hadoop
## 1.1 Introduction and install Hadoop on your desktop
```sql
SELECT movie_id, count(movie_id) as ratingCount
FROM ratings
GROUP BY movie_id
ORDER BY ratingCount
DESC;
```

```sql
SELECT name
FROM movie_names
WHERE movie_id = 50;
```

## 1.2 Hadoop overview and history
![What is Hadoop](./images/what-is-hadoop.png)

* Google w/GFS & MapReduce in 2003–2004
* Hadoop in 2006
* Why Hadoop?
    * data is too big
    * vertical scaling doesn't cut it
    * tolerant of hardware failure
    * horizontal scaling is linear
    * useful for more than batch operations
  
![Hadoop History](./images/world-of-hadoop.png)
## 1.3 Overview of the Hadoop Ecosystem
### Core Hadoop Ecosystem
* pink items are core Hadoop components, orange are elements that have grown up as part of the ecosystem to solve various problems
![World of Hadoop](./images/world-of-hadoop.png)

* HDFS:  Hadoop Distributed File System
    * logical disk distributed over physical devices
    * redundancy
* YARN:  Yet Another Resource Negotiator
    * distributes work across the Hadoop cluster
    * facilitates workloads such as ...
* MapReduce
    * a programming model that allows operations to be distributed across a cluster; simple but versatile
    * mappers:  transform data
    * reducers:  aggregate transformed data together
* Pig
    * high-level programming API that can be used to generated MapReduce workloads
    * SQL-like
* Hive
    * SQL queries on the distributed file store
* Apache Ambari
    * view/visualize your cluster
    * HortonWorks
* Mesos
    * Alternative to YARN
* Spark
    * ~ similar level as MapReduce
    * run queries on your data
    * Python, Scala (preferred)
    * ML, streaming data, SQL queries
* TEZ
    * similar to Spark
    * usually used to accelerate Hive
* Apache HBase
    * NoSQL DB
    * OLTP transactions
    * expose data from your Hadoop system to be consumed by other systems
* Apache Storm
    * processing streaming data/real-time
* Oozie
    * Job scheduler
* Zookeeper
    * cluster state coordinator/tracker
* Data Ingestion
    * Sqoop:  connector between JDBC/ODBC DBs and Hadoop
    * Flume:  web logs => Hadoop
    * Kafka:  general purpose tool for collecting data into Hadoop

### External Data Storage
![External Data Storage](./images/external-data-storage.png)

* MySQL (or other SQL DBs)
* Cassandra
* MongoDB
* like HBase, Cassandra & MongoDB are good options for "vending" data out of your cluster into other applications
* *NOTE:  he describes Cassandra, MongoDB & Apache HBase as 'columnar datastores'*

### Query Engines
![Query Engines](./images/query-engines.png)

* Apache Drill:  run queries against a wide range of NoSQL datastores
* Hue:  Cloudera's equivalent of Ambari
* Apache Phoneix:  similar to Drill, adds ACID guarantees, OLTP
* Presto
* Apache Zeppelin
* Hive technically falls into this group

# 2. Using Hadoop core:  HDFS and MapReduce
## 2.1 HDFS:  what it is and how it works

* Handles big files into blocks (128 MB by default)
* Blocks are replicated across multiple hosts

### HDFS Architecture

* Name Node:  single host that tracks all block locations
* Data Nodes:  store blocks & communicate w/each other to replicate blocks 
* Reading a File: 
    1. Client node reads locations from the Name Node
    1. Client node reads the blocks from the Data Nodes that are most efficient 
* Writing a File: 
    1. Communicate with the Name Node which establishes where the blocks will be stored
    1. The Client Node communicates with a Data Node to write the file
    1. The Client Nodes communicate with each other in order to replicate the blocks for redundancy
    1. The Data Nodes will confirm to the Client Node that data has been successfully written 
    1. The Client Node will inform the Name Node that the data has been written

* Dealing with lack of redundancy of a Name Node
    * Back up Metadata:  Name Node write logs locally and to a network partition
    * Run a Secondary Name Node:  Maintains a merged copy of edit log that can be used to restore; not an *functional* Name Node
    * HDFS Federation:  Each Name Node manages a specific, namespace volume
    * HDFS High Availability:  
        * Hot standby Name Node using shared, edit log
        * Zookeeper keeps track of which Name Node is active

### Using HDFS

* UI/Ambari
* CLI
* HTTP/HDFS proxies
* Java interfaces
* NFS Gateway

## 2.2 Install the MovieLens dataset into HDFS using the Ambari UI

* In Ambari's left column, choose "Files View" from the grid icon in the menu bar

## 2.3 MapReduce:  What is is and how it works

* Distributes the processing of data on your cluster
* Divides your data up into partitions that are mapped/transformed and reduced/aggregated by mapper and reducer functions you define
* Resilient to failure:  an application master monitors your mappers and reducers on each partition
* Mapper converts raw source data into key/value pairs
* Strip out unnecessary data as early as possible to avoid passing it around where it isn't needed
* "Shuffle and Sort":  MapReduce sorts and groups the mapped data, e.g. `[123:abc, 456:xyz, 123:def]` becomes `[123:[abc,def], 456:[xyz]]`; reducers operate on this grouped output

![MapReduce workflow](./images/putting-it-all-together.png)

## 2.4 How MapReduce distributes processing
![What's Happening](./images/whats-happening.png)

* Mapping operations which take place on different nodes but share the same key are merged together during Shuffle and Sort.
* MapReduce is natively Java
* Streaming (stdin/stdout) allows interfacing with other languages, e.g. Python
* Handling failure
    * Application master monitors worker tasks for errors or hanging
        * Restarts as needed
        * Preferably on a different node
    * What if the application master goes down? 
        * YARN can try to restart it
    * What if an entire node goes down?
        * This could be the application master
        * The resource manager will try to restart it
    * What if the resource manager goes down? 
        * Can set up "high availability" (HA) using Zookeeper to have a hot standby
* MapReduce has been partially superseded by tools like Spark or tools like Hive that support SQL-like queries

## 2.5 MapReduce example - Break down movie ratings by rating score
```python
def mapper_get_ratings(self, _, line):
  (userID, movieID, rating, timestamp) = line.split('\t')
  yield rating, 1
```
```python
def reducer_count_ratings(self, key, values):
    yield key, sum(values)
```

* Use the MRJob Python package
* Running an MRJob Python script w/out Hadoop: `python RatingsBreakdown.py u.data`
* Running with Hadoop:  `python RatingsBreakdown.py -r hadoop --hadoop-streaming-jar /usr/hdp/current/hadoop-mapreduce-client/hadoop-streaming.jar u.data`. The `--hadoop-streaming-jar` portion is not usually necessary on an AWS-EC2- or EMR-based jobs

## 2.6 Challenge Exercise
Count up ratings given for each movie
- All you need is to change one thing in the mapper - we don't care about ratings now, we care about movid ID's!
- Start with this and make sure you can do it.

**Stretch Goal**

* Sort movies by their numbers of ratings
* Strategy: 
    * Map to `(movidID, 1)` key/value pairs
    * Reduce with output of `(ratingCount, movieID)`
    * Send this to a second reducer so we end up with things sorted by rating count
* Gotchas:
    * How do we set up more than one MapReduce step?
    * How do we ensure the rating counts are sorted properly?
* Chain map/reduce stages together:

```python
def steps(self):
    return [
        MRStep(mapper=self.mapper_get_ratings,
               reducer=self.reducer_count_ratings),
        MRStep(reducer=self.reducer_sorted_output)
    ]
```

* Ensuring Proper Sorting
    * All values are treated as strings with streams and they are sorted as strings; zero-pad the numbers so they're sorted correctly:

```python
def reducer_count_ratings(self, key, values):
    yield str(sum(values)).zfill(5), key
```

# 2. Programming Hadoop with Pig
## 2.1. Introducing Ambari

* The Swiss Army Knife of your Hadoop cluster
* Dashboard:  what's going on in your Hadoop cluster e.g. health/monitoring
* Many installations start with Ambari first
* Services ... 
* Hosts:  what hosts are part of your cluster & the status of services are running on them
* Alerts
* Admin:  accounts and what's installed
* The Grid icon:  


### Enabling the `admin` account:  
```bash
$ su root
# ambari-admin-password-reset
```

# 3. Programming Hadoop with Pig
## 3.1. Introducing Pig

* Pig simplifies MapReduce operations via a scripting language
* "Pig Latin" uses a SQL-like syntax to define your map and reduce steps
* Supports user-defined functions (UDF's)
* Pig can also run on top of Tez, not just MapReduce
* Pig can be run standalone, as well

![Pig Architecture](./images/pig-architecture.png)

### Running Pig
 
* REPL-like Grunt
* script
* Ambari / Hue

## 3.2. Example:  Find the oldest, 5-star movies
![Creating a relation with Pig](./images/pig-relation.png)

* Pig defines a schema on the fly with `AS` 
* `chararray`, `int`
* "bags"? 
* `DUMP`:  prints out the entire loaded thing out

![Creating a relation from another relation](./images/pig-foreach-generate.png)
![Pig GROUP BY](./images/pig-group-by.png)

* Group By is similar to a Reduce operation

![A group](./images/pig-group.png)

 * `DESCRIBE` to print schema definitions
 
![Pig FILTER operation](./images/pig-filter.png)
![Pig JOIN operation](./images/pig-join.png)
![Pig ORDER BY operation](./images/pig-order-by.png)
![Putting It All Together](./images/pig-putting-it-all-together.png)

* Using the store command would be more appropriate in order to get data out of a completed Pig job

## 3.3. More Pig Latin
### Things you can do to a relation

* `LOAD`, `DUMP`, `STORE`
    * `STORE ratings INTO 'outRatings' USING PigStorage(':');`
* `FILTER`, `DISTINCT`, `FOREACH`/`GENERATE`, `MAPREDUCE` (call out to MapReduce operations), `STREAM` (like MapReduce console streaming), `SAMPLE`
* `JOIN`, `COGROUP` (different organization than `JOIN`), `GROUP`, `CROSS` (cartesian product), `CUBE`
* `ORDER`, `RANK`, `LIMIT` (return the first *N* rows; good for development/debugging)
* `UNION`, `SPLIT`

### Diagnostics

* `DESCRIBE`
* `EXPLAIN`
* `ILLUSTRATE`

### UDF's
Involves writing Java code, etc.

* `REGISTER`
* `DEFINE`
* `IMPORT`

### Other functions and loaders
* `AVG`, `CONCAT`, `COUNT`, `MAX`, `MIN`, `SIZE`, `SUM`

### Storage Classes
* PigStorage
* TextLoader
* JsonLoader
* AvroStorage
* ParquetLoader (column-oriented)
* OrcStorage
* HBaseStorage

### Other Pig resources
* [Programming Pig](https://learning.oreilly.com/library/view/programming-pig-2nd/9781491937082/) (O'Reilly)
* [Hadoop:  The Definitive Guide](https://learning.oreilly.com/library/view/hadoop-the-definitive/9781491901687/), ch. 16 (O'Reilly)

## 3.4. Challenge:  Find the most-rated one-star movie
* All movies w/avg. rating < 2.0
* Sort them by the total number of ratings
* Similar to "oldies but goodies"; use `COUNT()` rather than `AVG()`, i.e. `COUNT(ratings.rating)` to get the total number of ratings for a given group's bag

# 4. Programming Hadoop with Spark
## 4.1. Why Spark? 

* Apache Spark:  "a fast and general engine for large-scale data processing"
* Use "real" programming languages like Python, Java, Scala

![Spark is Scalable](./images/spark-its-scalable.png)
* Can use its own cluster manager
* Spark's cache is in-memory rather than on disk like HDFS
* Can be as much as 100x faster than Hadoop MapReduce (10x for on-disk)
* DAG Engine (direct acyclic graph) optimizes workflows, similar to Tez
* Main concept:  Resilient Distributed Dataset (RDD); Spark 2.0 introduces a Dataset which is more SQL-like
* [ ] TODO add "Components of Spark" screen shot
* Spark SQL is growing in popularity
* Benefits of using Scala (which he recommends)
    * Scala's functional programming model is a good fit for distributed processing
    * Better performance of a compiled language
    * Less code/boilerplate than Java
    * Python is slow by comparison

## 4.2. The Resilient Distributed Dataset

* Creating an RDD
    * via a `SparkContext`
        * responsible for making RDDs resilient & distributed
        * the Spark Shell creates an `sc` object for you
    * `nums = paralleliz([1, 2, 3, 4])`
    * `sc.textFile("[file://|s3n://|hdfs://]/path/to/file")`
    * `hiveCtx = HiveContext(sc) rows = hiveCtx.sql("SELECT name, age FROM users")`
    * Can also create from:
        * JDBC
        * Cassandra
        * HBase
        * Elasisearch
        * JSON, CSV, sequence files, object files, various compressed formats
* Transforming RDDs
    * `map`
    * `flatmap`
    * `filter`
    * `distinct`
    * `sample`
    * `union`, `intersection`, `subtract`, `cartesian`
    * Example
```python
rdd = sc.parallelize([1, 2, 3, 4])
squaredRDD = rdd.map(lambda x: x*x)
``` 
* RDD Actions
    * `collect & Spark 2`
    * `count`
    * `countByValue`
    * `take` useful for debugging
    * `top` useful for debugging
    * `reduce`
    * ... and more ... 
    * Thanks to Spark's lazy execution model, nothing will actually execute until an RDD Action is called
    
[Ambari Spark & Spark 2 setup](https://www.udemy.com/the-ultimate-hands-on-hadoop-tame-your-big-data/learn/v4/t/lecture/6082670?start=0)

### Running Spark Python scripts using Spark Submit
You can't simply use Python to run Spark scripts. 
`spark-submit LowestRatedMovieSpark.py` 
Check out `spark-submit --help` for options.

## 4.3. Datasets and Spark 2.0
* Extends RDD to a "DataFrame" object
* DataFrames
    * Contain Row objects
    * Can run SQL queries
    * Has a schema (leading to more efficient storage)
    * Read and write to JSON, Hive, parquet
    * Communicates with JDBC/ODBC, Tableau
* Using SparkSQL in Python
```python
from pyspark.sql import SQLContext, Row
hiveContext = HiveContext(sc)
inputData = spark.read.json(dataFile)
inputData.createOrReplaceTempView("myStructuredStuff")
myResultDataFrame = hiveContext.sql("""SELECT foo FROM bar ORDER BY foobar""")
```

### Other stuff you can do with DataFrames
* `myResultDataFrame.show()`
* `myResultDataFrame.select("someFieldName")`
* `myResultDataFrame.filter(myResultDataFrame("someFieldName" > 200)`
* `myResultDataFrame.groupBy(myResultDataFrame("someFieldName")).mean()`
* `myResultDataFrame.rdd().map(mapperFunction)` get the underlying RDD 

### Datasets
* A DataFrame is a Dataset of Row objects 
* Datasets can wrap known, typed data, too; not as useful in Python which is dynamically typed.
* The Spark 2.0 way is to use Datasets instead of DataFrames when you can
* New Spark developments converge on the Dataset API, e.g. ML 

![Spark Shell Access](./images/spark-shell-access.png)
### User-defined functions (UDF's)
```python
from pyspark.sql.types import IntegerType
hiveCtx.registerFunction("square", lambda x: x*x, IntegerType())
df = hiveCtx.sql("SELECT square('someNumericField') FROM tableName")
```

## 4.4. Find the Movie with the Lowest, Average Rating - with DataFrames
* `SparkSession`:  
    * open once and leave it open until you're done with it
    * includes both a `SparkContext` and a `SQLContext`
    * `getOrCreate`:  can pick up from where a failed session left off previously
    
### Configure HortonWorks env. to use Spark 2
`export SPARK_MAJOR_VERSION=2`

`spark-submit LowestRatedMovieDataFrame.py`

## 4.5. Movie Recommendations with MLLib
ALS is a recommendation engine.

## 4.6. Filter the lowest-rated movies by number of ratings
### The problem
* Our examples of finding the lowest-rated movies were polluted with movies only rated by one or two people
* Modify one or both of these scripts to only consider movies with at least ten ratings
    * [x] [update the RDD-based script](./notes/lowest-rated-movie-spark-output-diff.md)
    * [x] [update the DataFrame-based script](./notes/lowest-rated-movie-dataframe-output-diff.md)
    
# 9. Feeding Data to your Cluster
## 9.1. Kafka explained
### Two Problems
1. How to get data from many different sources flowing into your cluster
1. Processing it when it gets there

### Enter Kafka
* General-purpose publish/subscribe messaging system
* Servers store all incoming messages from *publishers* for some period of time, and *publishes* them to a stream of data called a *topic* 
* Kafka *consumers* subscribe to one or more topics, and receive data as it's published
* A stream / topic can have many, different consumers, all with their own position in the stream maintained
* Not just for Hadoop
![Kafka Architecture](./images/kafka-architecture.png)
* *Stream processors* transform data as it comes in
![How Kafka Scales](./images/kafka-scaling.png)

## 9.2. Setting up Kafka, Publishing some Data
```
[maria_dev@sandbox-hdp kafka-broker]$ pwd
/usr/hdp/current/kafka-broker
[maria_dev@sandbox-hdp kafka-broker]$ ls
bin  conf  config  doc  examples  libs  LICENSE  logs  NOTICE  pids
```

### Create a Topic
```
[maria_dev@sandbox-hdp bin]$ ./kafka-topics.sh --create --zookeeper sandbox-hdp.hortonworks.com:2181 --replication-factor 1 --partitions 1 --topic topic-one
```

### Listing Topics
```
[maria_dev@sandbox-hdp bin]$ ./kafka-topics.sh --list --zookeeper sandbox-hdp.hortonworks.com:2181 
ATLAS_ENTITIES
ATLAS_HOOK
__consumer_offsets
topic-one
```
### Publishing Messages to the Topic from the Console
```
[maria_dev@sandbox-hdp bin]$ ./kafka-console-producer.sh --broker-list sandbox-hdp.hortonworks.com:6667 --topic topic-one
```

### Consuming Topic Messages from the Console
```
[maria_dev@sandbox-hdp bin]$ ./kafka-console-consumer.sh --bootstrap-server sandbox-hdp.hortonworks.com:6667 --topic topic-one --from-beginning
```

## 9.3. Publishing Web Logs with Kafka
### Files involved in the setup
* File location is `/usr/hdp/current/kafka-broker/config`
* `connect-standalone.properties`:  set up the network environment
* `connect-console-sink.properties`:   how we're going to store the messages on the stream
* `connect-console-source.properties`:  listen to changes on a given file & publish them on a topic

