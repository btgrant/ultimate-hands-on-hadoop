# Original Output
```
('Hostile Intentions (1994)', 1, 1.0)
('Amityville: A New Generation (1993)', 5, 1.0)
('Lotto Land (1995)', 1, 1.0)
('Amityville: Dollhouse (1996)', 3, 1.0)
('Careful (1992)', 1, 1.0)
('Further Gesture, A (1996)', 1, 1.0)
('Power 98 (1995)', 1, 1.0)
('Falling in Love Again (1980)', 2, 1.0)
('Low Life, The (1994)', 1, 1.0)
('Touki Bouki (Journey of the Hyena) (1973)', 1, 1.0)
```

# Filtered Output
```
before filtering count is 1682
after filtering count is 1119
('Children of the Corn: The Gathering (1996)', 19, 1.3157894736842106)
('Body Parts (1991)', 13, 1.6153846153846154)
('Amityville II: The Possession (1982)', 14, 1.6428571428571428)
('Lawnmower Man 2: Beyond Cyberspace (1996)', 21, 1.7142857142857142)
('Robocop 3 (1993)', 11, 1.7272727272727273)
('Free Willy 3: The Rescue (1997)', 27, 1.7407407407407407)
("Gone Fishin' (1997)", 11, 1.8181818181818181)
('Solo (1996)', 12, 1.8333333333333333)
('Vampire in Brooklyn (1995)', 12, 1.8333333333333333)
('Ready to Wear (Pret-A-Porter) (1994)', 18, 1.8333333333333333)
```

