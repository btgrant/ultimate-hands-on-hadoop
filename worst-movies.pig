-- Load and organize the ratings data
ratings = LOAD '/user/maria_dev/ml-100k/u.data' AS (userID:int, movieID:int, rating:int, ratingTime:int);
-- ratings = LIMIT ratings 1000;
-- DUMP ratings;

ratingsByMovie = GROUP ratings BY movieID;
-- DESCRIBE ratingsByMovie;
-- DUMP ratingsByMovie;

movieIDWithRatingCount = FOREACH ratingsByMovie GENERATE group
    AS movieID,
    AVG(ratings.rating) AS avgRating,
    SIZE(ratings) AS numberOfRatings;
-- DUMP movieIDWithRatingCount;
-- DESCRIBE movieIDWithRatingCount;

badMovies = FILTER movieIDWithRatingCount BY avgRating < 2.0;

-- Now load the movie data
metadata = LOAD '/user/maria_dev/ml-100k/u.item' USING PigStorage('|')
    AS (movieID:int, movieTitle:chararray, releaseDate:chararray, videoRelease:chararray, imdbLink:chararray);

nameLookup = FOREACH metadata GENERATE movieID, movieTitle; -- , ToUnixTime(ToDate(releaseDate, 'dd-MMM-yyyy')) AS releaseTime;
--DESCRIBE nameLookup;


-- combine ratings and movie data
namedBadMovies = JOIN badMovies BY movieID, nameLookup BY movieID;
DESCRIBE namedBadMovies; -- {badMovies::movieID: int,badMovies::avgRating: double,badMovies::numberOfRatings: long,nameLookup::movieID: int,nameLookup::movieTitle: chararray}


finalResults = FOREACH namedBadMovies GENERATE
    nameLookup::movieTitle AS movieName,
    badMovies::avgRating AS avgRating,
    badMovies::numberOfRatings AS numRatings;

finalResultsSorted = ORDER finalResults BY numRatings DESC;
DUMP finalResultsSorted
